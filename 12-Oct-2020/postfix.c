/*  converts infix expression to post fix */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char stack[50];
int top = -1;

void push(char);
char pop();
int checkPrecedence(char);
void infix_postfix(char[], char[]);

void incorrect();
void credits();

int main(int argc, char const *argv[]) {
    char infix[50], postfix[50];

    printf("Enter an infix expression:\n");
    fgets(infix, 50, stdin);
    infix_postfix(infix, postfix);
    printf("\nPostfix expression:\n");
    printf("%s", postfix);

    getchar();
    credits();
    return 0;
}

void push(char item) {
    stack[++top] = item;
}

char pop() {
  return stack[top--];
}

int checkPrecedence(char a) {
    switch (a) {
        case '-':
        case '+': return 1;
        case '*':
        case '/': return 2;
        case '^': return 3;
        default : return -1;
    }
}

void infix_postfix(char infix[], char postfix[]) {
    char item, temp;
    int inCounter = 0, postCounter = 0;
    
    push('(');
    strcat(infix, ")");
    item = infix[inCounter];
    while (item != '\0') {
        if (isalpha(item) || isdigit(item)) postfix[postCounter++] = item; 
        else if (item == '(') push(item);
        else if (item == ')') {
            temp = pop(); 
            while (temp != '(') { 
                postfix[postCounter++] = temp;
                temp = pop();
            }
        } else {
            temp = pop();
            while (checkPrecedence(temp) > checkPrecedence(item)) { 
                postfix[postCounter++] = temp; 
                temp = pop();    
            }
            push(temp);
            push(item);
        }
        item=infix[++inCounter];
    }
    postfix[postCounter] = '\0';
}

void incorrect() {
    printf("Incorrect data. Program will exit\n");
    credits();    
}

void credits() {
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|              Github: akashroshan135               |\n");
    printf("+---------------------------------------------------+\n");
    exit(0);
}