/* outputs the sum of two matrices */

#include <stdio.h>
#include <stdlib.h>

void incorrect();
void credits();

int main(int argc, char const *argv[]) {
    int r, c, matrix1[50][50], matrix2[50][50], sum[50][50];

    printf("Enter the number of rows (between 2 and 50): ");
    if (scanf("%d", &r) != 1 || r < 2 || r > 50) incorrect();
    printf("Enter the number of columns (between 2 and 50): ");
    if (scanf("%d", &c) != 1 || c < 2 || c > 50) incorrect();

    printf("\nEnter the elements for first matrix\n");
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            printf("Enter element matrix1[%d][%d] = ", i + 1, j + 1);
            if (scanf("%d", &matrix1[i][j]) != 1) incorrect();
        }
    }
    printf("\nEnter the elements for second matrix\n");
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            printf("Enter element matrix2[%d][%d] = ", i + 1, j + 1);
            if (scanf("%d", &matrix2[i][j]) != 1) incorrect();
            sum[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    printf("\nFirst Matrix:\n");
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            printf("%d\t", matrix1[i][j]);
        }
        printf("\n");
    }
    printf("\nSecond Matrix:\n");
    for (int i = 0; i < r; ++i) {
        for (int j = 0; j < c; ++j) {
            printf("%d\t", matrix2[i][j]);
        }
        printf("\n");
    }
    printf("\nSum of the two Matrices:\n");
    for (int i = 0; i < r; ++i) {
        for (int j = 0; j < c; ++j) {
            printf("%d\t", sum[i][j]);
        }
        printf("\n");
    }

    getchar();
    credits();
    return 0;
}

void incorrect() {
    printf("Incorrect input. Program will exit\n");
    credits();    
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
    exit(0);
}