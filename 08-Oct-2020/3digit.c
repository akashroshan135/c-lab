/*  
    outputs sum of digits of first no, product of
    digits of second no and sum of both results
*/

#include <stdio.h>
#include <stdlib.h>

void incorrect();
void credits();

int main(int argc, char const *argv[]) {
    int num1, num2, n, sum = 0, product = 1;

    printf("Enter a positive number (3 digits or more)       = ");
    if (scanf("%d", &num1) != 1 || num1 < 100) incorrect();
    printf("Enter another positive number (3 digits or more) = ");
    if (scanf("%d", &num2) != 1 || num1 < 100) incorrect();

    n = num1;
    while (n > 0) {  
        sum += (n % 10);  
        n = n / 10;  
    }
    n = num2;
    while (n > 0) {  
        product *= (n % 10);  
        n = n / 10;  
    }
    n = sum + product;

    printf("\nThe sum of the digits of %d = %d", num1, sum);
    printf("\nThe products of the digits of %d = %d", num2, product);
    printf("\nThe sum of the two results = %d", n);
    

    getchar();
    credits();
    return 0;
}

void incorrect() {
    printf("Incorrect input. Program will exit\n");
    credits();    
}

void credits() {
    printf("\n\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
    exit(0);
}