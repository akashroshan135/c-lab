/* reverse a string using stack */

#include <stdio.h>
#include <string.h>
char string[50], stack[50];
int len, top = -1;

void stringReverse();
void push(char);
char pop();

int main() {
    printf("\nReverse a string using stack program using C\n");
    printf("\t By: Akash Roshan A 2047207\n");

    printf("\nPlease enter a string (max lenght = 50) : ");
    fgets(string, 50, stdin);
    len = strlen(string);

    printf("\nString before reversing:\n%s\n", string);
    stringReverse();
    printf("String after reversing:%s\n", string);

}

void stringReverse(){
    for (int i = 0; i < len; i++) push(string[i]);
    for (int i = 0; i < len; i++) string[i] = pop();
}

void push(char item) {
    stack[++top] = item;
}
char pop() {
    return(stack[top--]);
}
