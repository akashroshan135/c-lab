/* partition the array into two sub-arrays having the same sum of elements. */

/* reverse a string using stack */

#include <stdio.h>
#include <stdlib.h>

void incorrect();
int partition(int[], int);

int main() {
    printf("\nArray partition program using C\n");
    printf("  By: Akash Roshan A 2047207\n");

    int array[50], n;
    printf("\nPlease no of elements : ");
	if (scanf("%d", &n) != 1) incorrect();
    printf("\nPlease enter 5 numbers : \n");
    for (int i = 0; i < 5; i++) if (scanf("%d", &array[i]) != 1) incorrect();

    int partitionPoint  = partition(array, n);

	printf("\nFirst Subset\n");
	for (int i = 0; i < partitionPoint; i++) printf("%d\t", array[i]);

	printf("\nSecond Subset\n");
	for (int i = partitionPoint; i < 5; i++) printf("%d\t", array[i]);

	printf("\n");
}

void incorrect() {
    printf("Incorrect input. Program will exit\n");
	exit(0);
}

int partition(int array[], int n) {
	for (int i = 0; i < n; i++) {
		int sumLeft = 0;
		for (int j = 0; j < i; j++) sumLeft += array[j];

		int sumRight = 0;
		for (int j = i; j < n; j++) sumRight += array[j];

		if (sumLeft == sumRight) return i;
	}
	return -1;
}
