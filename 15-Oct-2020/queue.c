//Queue stuff
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int queue[50], front = -1, rear = -1, n;

void enqueue(int);
int dequeue();
void display();

void incorrect();
void credits();


int main(int argc, char const *argv[]) {
    int choice, item;
    system("clear");
    printf("Enter the size of the queue (Between 3 and 50) = ");
    if (scanf("\n%d", &n) != 1 || n < 3 || n > 50) incorrect();
    printf("This queue can hold %d element(s)\n", n);

    printf("\nPress Enter to continue...");
    getchar();
    getchar();
    system("clear");

    do {

        printf("\tQueue Operations\n\n");
        printf("1. Enqueue\n2. Dequeue\n3. Display Queue\n4. Exit\n\nEnter your choice = ");
        
        scanf("%d", &choice);
        switch (choice) {
            case 1: printf("\nEnter the element to add = ");
                    if (scanf("%d", &item) != 1) incorrect();
                    enqueue(item);
                    display();
                    break;
            case 2: item = dequeue();
                    printf("\nDeleted element = %d\n", item);
                    display();
                    break;
            case 3: display();
                    break;
            case 4: printf("\nThank you for using the program\n");
                    credits();
                    break;
            default:printf("\nIncorrect input. Please input the correct number\n");
        }
        
        printf("\nPress Enter to continue...");
        getchar();
        getchar();
        system("clear");
    
    } while(choice != 4);
    return 0;
}

void display() {
    if (front == -1 || rear == -1) {
        printf("\nThe Queue is empty\n");
        printf("You can enter %d element(s)\n", n);
    }
    else {
        printf("\nThe contents of Queue are:\n");
        for (int i = front; i <= rear; i++) printf("%d\t", queue[i]);
        printf("\nYou can enter %d element(s)\n", n - rear - 1);
    }
}

void enqueue(int item) {
    if (rear == n - 1) {
        printf("\nQueue Overflow. The program will now exit\n");
        credits();
    } else {
        queue[++rear] = item;
        if (front == -1) front = 0;
    }
}

int dequeue() {
    int item;
    if (front == -1) {
        printf("\nQueue Underflow. The program will now exit\n");
        credits();
    } else {
        item = queue[front];
        if (front == rear) front = rear = -1;
        else front++;
    }
    return item;
}

void incorrect() {
    printf("Incorrect input. The program will now exit\n");
    credits();    
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|              Github: akashroshan135               |\n");
    printf("+---------------------------------------------------+\n");
    exit(0);
}