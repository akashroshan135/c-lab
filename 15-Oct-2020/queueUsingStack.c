// Queue stuff using stacks

#include <stdio.h>
#include <stdlib.h>

/*  
    stack1 is used as the queue
    stack2 is used to dequeue an element
*/
int stack1[50], stack2[50], top1 = -1, top2 = -1;
int front = -1, rear = -1, queuesize;

void push1(int);
void push2(int);
int pop1();
int pop2();

void enqueue(int);
int dequeue();
void display();

void incorrect();
void credits();


int main(int argc, char const *argv[]) {
    int choice, item;
    
    system("clear");
    printf("Enter the size of the queue (Between 3 and 50) = ");
    if (scanf("\n%d", &queuesize) != 1 || queuesize < 3 || queuesize > 50) incorrect();
    printf("This queue can hold %d element(s)\n", queuesize);

    printf("\nPress Enter to continue...");
    getchar();
    getchar();
    system("clear");

    do {

        printf("\tQueue Operations\n\n");
        printf("1. Enqueue\n2. Dequeue\n3. Display Queue\n4. Exit\n\nEnter your choice = ");
        
        scanf("%d", &choice);
        switch (choice) {
            case 1: printf("\nEnter the element to add = ");
                    if (scanf("%d", &item) != 1) incorrect();
                    enqueue(item);
                    display();
                    break;
            case 2: item = dequeue();
                    printf("\nDeleted element = %d\n", item);
                    display();
                    break;
            case 3: display();
                    break;
            case 4: printf("\nThank you for using the program\n");
                    credits();
                    break;
            default:printf("\nIncorrect input. Please input the correct number\n");
        }
        
        printf("\nPress Enter to continue...");
        getchar();
        getchar();
        system("clear");
    
    } while(choice != 4);
    return 0;
}

// push and pop operation for first stack
void push1(int item) {
    stack1[++top1] = item;
}
int pop1() {
    return(stack1[top1--]);
}
// push and pop operation for second stack
void push2(int item) {
    stack2[++top2] = item;
}
int pop2() {
    return(stack2[top2--]);
}


void display() {
    if (front == -1 || rear == -1) {
        printf("\nThe Queue is empty\n");
        printf("You can enter %d element(s)\n", queuesize);
    }
    else {
        printf("\nThe contents of Queue are:\n");
        for (int i = 0; i <= top1; i++) printf("%d\t", stack1[i]);
        printf("\nYou can enter %d element(s)\n", queuesize - rear - 1);
    }
}

void enqueue(int item) {
    if (rear == queuesize - 1) {
        printf("\nQueue Overflow. The program will now exit\n");
        credits();
    }
    push1(item);
    rear++;
    if (front == -1) front = 0;
}

int dequeue() {
    if (front == -1) {
        printf("\nQueue Underflow. The program will now exit\n");
        credits();
    }
    int count = rear + 1 - front;
    for (int i = 0; i < count; i++) push2(pop1());
    int item = pop2();
    count--;
    for (int i = 0; i < count; i++) push1(pop2());
    if (front == rear) front = rear = -1;
    else front++;
    return item;
}


void incorrect() {
    printf("\nIncorrect input. The program will now exit\n");
    credits();    
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|              Github: akashroshan135               |\n");
    printf("+---------------------------------------------------+\n");
    exit(0);
}