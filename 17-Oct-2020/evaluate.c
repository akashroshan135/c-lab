#include <stdio.h>

int main()
{
	int i;

	printf("Type an integer value: ");
	scanf("%d",&i);
	evaluate(i);

	return(0);
}

void evaluate(int x)
{
	if (x > 10) printf("The number %d is greater than 10", x);
    else if (x == 10) printf("The number %d is equal to 10", x);
    else printf("The number %d is lesser than 10", x);
}

