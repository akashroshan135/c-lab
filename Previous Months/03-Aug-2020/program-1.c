/* demonstrates different datatypes and their sizes*/

#include<stdio.h>
#include<stdlib.h>

void credits();

int main(int argc, char const *argv[])
{
    int i = 10;
    float f = 39.52;
    long l = 21473254;    
    double d = 4244.546000;
    char c = 'f';

    printf("Interger i.         Value = %d \t Size = %lu byte.\n", i, sizeof(i)); 
    printf("Floating Point f.   Value = %f \t Size = %lu byte.\n", f, sizeof(f)); 
    printf("Long l.             Value = %ld \t Size = %lu byte.\n", l, sizeof(l)); 
    printf("Double d.           Value = %lf \t Size = %lu byte.\n", d, sizeof(d)); 
    printf("Character c.        Value = %c \t Size = %lu byte.\n", c, sizeof(c)); 

    getchar();
    credits();
    return 0;
}

void credits() {
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}