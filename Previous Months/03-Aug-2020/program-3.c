#include<stdio.h>
#include<stdlib.h>

void credits();

int main(int argc, char const *argv[])
{
    int a = 20, b = 10, c;

    /*--------------------------------------*/

    printf("Arithmetic operators\n\n");
    printf("a = %d\tb = %d\n", a, b);
    
    printf("+ Operatior\n");
    c = a + b;
    printf("Result = %d\n", c);

    printf("- Operatior\n");
    c = a - b;
    printf("Result = %d\n", c);
    
    printf("* Operatior\n");
    c = a * b;
    printf("Result = %d\n", c);

    printf("/ Operatior\n");
    c = a / b;
    printf("Result = %d\n", c);

    printf("% Operatior\n");
    c = a % b;
    printf("Result = %d\n", c);

    getchar();
    system("clear");

    /*--------------------------------------*/

    printf("a = %d\tb = %d\n", a, b);


    printf("++ Operatior\n");
    a++;
    printf("Result of a = %d\n", a);
    
    printf("-- Operatior\n");
    a--;
    printf("Result of a = %d\n", a);

    getchar();
    system("clear");

    /*--------------------------------------*/

    printf("Assignment operators\n\n");
    printf("a = %d\tb = %d\n", a, b);
    
    printf("+= Operatior\n");
    a += b;
    printf("Result of a = %d\n", a);

    printf("-= Operatior\n");
    a -= b;
    printf("Result of a = %d\n", a);
    
    printf("*= Operatior\n");
    a *= b;
    printf("Result of a = %d\n", a);

    printf("/= Operatior\n");
    a /= b;
    printf("Result of a = %d\n", a);

    getchar();
    system("clear");

    /*--------------------------------------*/

    printf("Logical operators\n\n");

    a = 1, b = 0;
    printf("a = %d\tb = %d\n", a, b);

    c = (a && b);
    printf("Logical AND = %d\n", c);    
    
    c = (a || b);
    printf("Logical OR = %d\n", c);    
    
    printf("Logical NOT of a = %d\n", !a);

    getchar();
    system("clear");

    /*--------------------------------------*/

    credits();
    return 0;
}

void credits() {
    system("clear");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}