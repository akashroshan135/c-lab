/* demonstrates different control statements */

#include<stdio.h>
#include<stdlib.h>

void simpleIf();
void ifElse();
void ifElseIf();
void nestedIf();
void whileLoop();
void forLoop();
void credits();

int main(int argc, char const *argv[])
{
    int ch;
    do
    {
        printf("\n1. Simple-If\n2. If-Else\n3. If-Else-If\n4. Nested-If\n5. While Loop\n6. For Loop\nAny other to exit\n");
        printf("choose an option  = ");
        scanf("%d", &ch);
        system("clear");
        switch (ch) {
            case 1: simpleIf();
                    break;
            case 2: ifElse();
                    break;
            case 3: ifElseIf();
                    break;
            case 4: nestedIf();
                    break;
            case 5: whileLoop();
                    break;
            case 6: forLoop();
                    break;
            default:break;
        }
    } while (ch < 7);
    credits();
    return 0;
}

void simpleIf() {
    int a = 5;
    printf("a = %d\n", a);
    if (a > 10) {
        printf("a is above 10\n");
    }
}

void ifElse() { 
    int b = 75;
    printf("b = %d\n", b);
    if (b < 50) {
        printf("b is below 50\n");
    } else {
        printf("b is above 50\n");
    }
}

void ifElseIf() {
    int c = 20;
    if(c == 10) {
        printf("Value of a is 10\n");
    } else if (c == 20) {
        printf("Value of a is 20\n");
    } else if( c == 30 ) {
        printf("Value of a is 30\n" );
    } else {
      printf("None of the values is matching\n" );
    }
}   

void nestedIf() {
    printf("greatest of three nos\n");
    int x = 25, y = 35, z = 30, great;
    printf("x = %d, y = %d, z = %d\n", x, y, z);
    if (x > y) {
        if (x > z) {
            great = x;
        } else {
            great = z;
        }        
    } else {
        if (y > z) {
            great = y;
        } else {
            great = z;
        }
    }
    printf("greater of three nos = %d\n", great);
}

void whileLoop() {
    int d = 10;
    while(d < 20) {
        printf("value of d = %d\n", d);
        d++;
    }
}

void forLoop() {
    int i;
    for (i = 1; i < 11; ++i)
    {
        printf("%d\t", i);
    }
    printf("\n");
}

void credits() {
    system("clear");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}