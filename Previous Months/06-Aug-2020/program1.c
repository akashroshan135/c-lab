#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int i = 3, *j;
    j = &i;
    printf("int i = %d\n", i);
    printf("int j = %d\n", j);
    printf("address i = %d\n", &i);
    printf("address j = %d\n", &j);
    printf("int i via j = %d\n", *j);
    return 0;
}
