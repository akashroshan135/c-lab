/* Passing Pointer to a Function */

#include <stdio.h>

void moresalary(int*, int);
void credits();

int main(int argc, char const *argv[]) {
    int salary=0, bonus=0;
    
    printf("Enter the current salary = "); 
    scanf("%d", &salary);
    printf("Enter bonus = ");
    scanf("%d", &bonus);
    
    moresalary(&salary, bonus);
    
    printf("Final salary = %d", salary);

    getchar();
    credits();
    return 0;
}

void moresalary(int *s, int b)
{
    *s = *s + b;
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}