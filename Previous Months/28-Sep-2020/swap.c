/* swaps numbers using pointers */ 

#include<stdio.h>

void swap_nos(int*, int*);
void credits();

int main()
{
	int no1, no2;
	printf("Enter two numbers : \n");
	scanf("%d%d", &no1, &no2);
	
    printf("Values before swapping\n");
	printf("A = %d, B = %d\n", no1, no2);
	
    swap_nos(&no1 ,&no2);
	
    printf("Values after swapping\n");
	printf("A = %d, B = %d\n", no1, no2);
	
    getchar();
    credits();
    return 0;
}

void swap_nos(int *x, int *y){
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}