/* custom made string copy function using a buffer*/

#include<stdio.h>
#include<string.h>

void credits();

int main(int argc, char const *argv[]) {
    char name[30], buffer[60], end[20] = "PASSED THE CHALLENGE";
    int i, l, l1, l2;
    
    printf("Enter lenght of name = ");
    scanf( "%d" ,&l);
    
    printf("Enter your name = ");

    for(i = 0; i <= l; i++) name[i]=getchar();
    name[i] = '\0';	
    
    strcpy(buffer,name);
    l1 = l + 2;
    buffer[l + 1] = ' ';
    l2 = strlen(end);
    
    for(int j = 0; j <= l2; j++) {
        buffer[l1] = end[j];
        l1++;
    }
    i = 0;
    while(buffer[i] != '\0') putchar(buffer[i++]);
    
    credits();
    return 0;
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}