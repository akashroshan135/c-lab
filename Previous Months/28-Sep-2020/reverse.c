/* read a line of text on a character-by-character basis, and then displays the characters in reverse order.  
Recursion is used to carry out the reversal of the characters */

#include <stdio.h>

void strreverse();
void credits();

int main(int argc, char const *argv[]) {
    printf("Enter a sentence : ");
    strreverse();

    getchar();
    credits();
    return 0;
}

void strreverse() {
    char c;
    scanf("%c", &c);
    if (c != '\n') {
        strreverse();
        printf("%c", c);
    }
}

void credits() {
    printf("\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}