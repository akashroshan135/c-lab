/* function to circularly shift their values to right */
#include <stdio.h>

void swap(int *, int *, int *);
void credits();

int main(int argc, char const *argv[]) {
    int x, y, z;
    printf("Enter three numbers :\n");
    scanf("%d", &x);
    scanf("%d", &y);
    scanf("%d", &z);

    printf("\nBefore shift:");
    printf("\nx = %d", x);
    printf("\ny = %d", y);
    printf("\nz = %d", z);
    swap(&x, &y, &z);
    printf("\nAfter shift:");
    printf("\ny = %d", y);
    printf("\nz = %d", z);
    printf("\nx = %d", x);

    credits();
    return 0;
}

void swap(int *a, int *b, int *c) {
    int temp;
    temp = *a;
    *a = *c;
    *c = *b;
    *b = temp;
}

void credits() {
    printf("\n\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("+---------------------------------------------------+\n");
}