/*Program to find Pth & Qth prime number and print their product*/
#include <stdio.h>

int getNPrime(int);
void credits();

int main(int argc, char const *argv[]) {
    int p, q, prime1, prime2, prod;
    printf("Enter the pth and qth value:\n");
    scanf("%d", &p);
    scanf("%d", &q);
    if (p >= 0 && q >= 0 && p <= 100 && q <= 100) {
        prime1 = getNPrime(p);
        prime2 = getNPrime(q);
        prod = prime1 * prime2;
    
        printf("\n%dth prime number = %d", p, prime1);
        printf("\n%dth prime number = %d", q, prime2);
        printf("\nProduct of the two primes = %d", prod);
    } else {
        printf("\nInvalid number or exceeded 100");
    }
    credits();
    return 0;
}

int getNPrime(int n) {
    int i, j, count=0, flag=0;
    for (i = 2; i > 0; i++) {
        flag = 0;
        for (j = 2; j < i; j++) {
            if (i % j == 0) {
                flag = 1;
                break;
            }
        }
        if (flag == 0) count++;
        if (count == n) return i;
    }
}

void credits() {
    printf("\n\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("+---------------------------------------------------+\n");
}