/* pushes all negative elements to the end of array */

#include <stdio.h>

int ordered[50];

void printArray(int[], int);
int *orderTheElements(int[], int);
void credits();

int main(int argc, char const *argv[]) {
    int n, array[50], *sorted , num;

    printf("Enter the no of elements (min: 3) = ");
    if (scanf("%d", &n) != 1 || n <= 2) {
        printf("Incorrect input. Program will exit");
        credits();
        return 0;
    }
    printf("Enter the elements : \n");
    for (int i = 0; i < n; i++) {
        if (scanf("%d", &num) == 1) array[i] = num;
        else {
            printf("Incorrect input. Program will exit\n");
            credits();
            return 0;
        }
    }

    printf("\nArray before sorting : \n");
    printArray(array, n);    

    sorted = orderTheElements(array, n);
    
    printf("\n\nArray after sorting : \n");
    printArray(sorted, n);

    getchar();
    credits();
    return 0;
}

void printArray(int array[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d\t", array[i]);
    }
}

int *orderTheElements(int array[], int n) {
    int orderedcount = 0;

    // inserts all postive nos and zero to the ordered list
    for (int i = 0; i < n; i++) {
        if (array[i] >= 0) ordered[orderedcount++] = array[i];
    }

    // returns if the array has only negative or only positve nos
    if (orderedcount == n) return ordered;
    else if (orderedcount == 0) return array;

    // inserts negative nos to the ordered list
    for (int i = 0; i < n; i++) {
        if (array[i] < 0) ordered[orderedcount++] = array[i];
    }

    return ordered;
}

void credits() {
    printf("\n\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}