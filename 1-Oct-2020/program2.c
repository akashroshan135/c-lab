/* checks for common elements in two arrays */

#include <stdio.h>

void printArray(int[], int);
void credits();

int main(int argc, char const *argv[]) {
    int n1, array1[50], n2, array2[50], common[50], commonCount = 0, num;

    printf("Enter the no of elements for first array (min: 3) = ");
    if (scanf("%d", &n1) != 1 || n1 <= 2){
        printf("Incorrect input. Program will exit");
        credits();
        return 0;
    }
    printf("Enter the elements for first array : \n");
    for (int i = 0; i < n1; i++) {
        if (scanf("%d", &num) == 1) array1[i] = num;
        else {
            printf("Incorrect input. Program will exit\n");
            credits();
            return 0;
        }
    }
    printf("Enter the no of elements for second array (min: 3) = ");
    if (scanf("%d", &n2) != 1 || n2 <= 2){
        printf("Incorrect input. Program will exit");
        credits();
        return 0;
    }
    printf("Enter the elements for second array : \n");
    for (int i = 0; i < n2; i++) {
        if (scanf("%d", &num) == 1) array2[i] = num;
        else {
            printf("Incorrect input. Program will exit\n");
            credits();
            return 0;    
        }
    }

    printf("\nFirst Array : \n");
    printArray(array1, n1);    
    printf("\nSecond Array : \n");
    printArray(array2, n2);    

    // finds the common elements
    for (int i = 0; i < n1; i++) {
        for(int j = 0; j < n2; j++) {
            if (array1[i] == array2[j]) common[commonCount++] = array1[i];
        }
    }    

    // sorts the array
    int temp;
    for (int i = 0; i < commonCount; i++) {
        for(int j = i + 1; j < commonCount; j++) {
            if (common[i] > common[j]) {
                temp = common[i];
                common[i] = common[j];
                common[j] = temp;
            }
        }
    }

    printf("\n\nCommon elements : \n");
    printArray(common, commonCount);    

    getchar();
    credits();
    return 0;
}

void printArray(int array[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d\t", array[i]);
    }
}

void credits() {
    printf("\n\n");
    printf("+---------------------------------------------------+\n");
    printf("|                   Akash Roshan A                  |\n");
    printf("|                      2047207                      |\n");
    printf("|                 Github: [REDACTED]                |\n");
    printf("+---------------------------------------------------+\n");
}